<?php
/**
 * Plugin Name: Author Social Networks
 */

define( 'TWITTER_SLUG', 'twitter' );
define( 'TWITTER_BRAND_NAME', 'Twitter' );
define( 'TWITTER_USER_FIELD', 'twitter-username' );
define( 'INSTAGRAM_SLUG', 'instagram' );
define( 'INSTAGRAM_BRAND_NAME', 'Instagram' );
define( 'INSTAGRAM_USER_FIELD', 'instagram-username' );
define( 'FACEBOOK_SLUG', 'facebook' );
define( 'FACEBOOK_BRAND_NAME', 'Facebook' );
define( 'FACEBOOK_USER_FIELD', 'facebook-username' );
define( 'TIKTOK_SLUG', 'tiktok' );
define( 'TIKTOK_BRAND_NAME', 'TikTok' );
define( 'TIKTOK_USER_FIELD', 'tiktok-username' );
define( 'TUMBLR_SLUG', 'tumblr' );
define( 'TUMBLR_BRAND_NAME', 'tumblr' );
define( 'TUMBLR_USER_FIELD', 'tumblr-username' );
define( 'POCKET_SLUG', 'pocket' );
define( 'POCKET_BRAND_NAME', 'Pocket' );
define( 'POCKET_USER_FIELD', 'pocket-username' );
define( 'PATREON_SLUG', 'patreon' );
define( 'PATREON_BRAND_NAME', 'Patreon' );
define( 'PATREON_USER_FIELD', 'patreon-username' );
define( 'MASTODON_SLUG', 'mastodon' );
define( 'MASTODON_BRAND_NAME', 'Mastodon' );
define( 'MASTODON_USER_FIELD', 'mastodon-url' );
define( 'SPOTIFY_SLUG', 'spotify' );
define( 'SPOTIFY_BRAND_NAME', 'Spotify' );
define( 'SPOTIFY_USER_FIELD', 'spotify-url' );
define( 'PLUGIN_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
define( 'PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

class Author_Social_Networks {
	private static $usernames_to_validate = array(
		array(
			'field' => TWITTER_USER_FIELD,
			'slug'  => TWITTER_SLUG,
			'name'  => TWITTER_BRAND_NAME,
		),
		array(
			'field' => INSTAGRAM_USER_FIELD,
			'slug'  => INSTAGRAM_SLUG,
			'name'  => INSTAGRAM_BRAND_NAME,
		),
		array(
			'field' => FACEBOOK_USER_FIELD,
			'slug'  => FACEBOOK_SLUG,
			'name'  => FACEBOOK_BRAND_NAME,
		),
		array(
			'field' => TIKTOK_USER_FIELD,
			'slug'  => TIKTOK_SLUG,
			'name'  => TIKTOK_BRAND_NAME,
		),
		array(
			'field' => TUMBLR_USER_FIELD,
			'slug'  => TUMBLR_SLUG,
			'name'  => TUMBLR_BRAND_NAME,
		),
		array(
			'field' => POCKET_USER_FIELD,
			'slug'  => POCKET_SLUG,
			'name'  => POCKET_BRAND_NAME,
		),
		array(
			'field' => PATREON_USER_FIELD,
			'slug'  => PATREON_SLUG,
			'name'  => PATREON_BRAND_NAME,
		),
	);

	private static $urls_to_validate = array(
		array(
			'field'  => MASTODON_USER_FIELD,
			'slug'   => MASTODON_SLUG,
			'name'   => MASTODON_BRAND_NAME,
			'domain' => '',
		),
		array(
			'field'  => SPOTIFY_USER_FIELD,
			'slug'   => SPOTIFY_SLUG,
			'name'   => SPOTIFY_BRAND_NAME,
			'domain' => 'spotify.com',
		),
	);

	public function init() {
		load_plugin_textdomain( 'author-social-networks', false, dirname( plugin_basename( __FILE__ ) ) );
		if ( is_admin() ) {
			// @todo if is user-edit.php
			wp_register_style( 'author-social-networks-admin-style', PLUGIN_URL . '/style-admin.css', array(), filemtime( PLUGIN_PATH . '/style-admin.css' ) );
			wp_enqueue_style( 'author-social-networks-admin-style' );
		}
		add_action( 'show_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 9 );
		add_action( 'edit_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 9 );
		add_action( 'personal_options_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
		add_action( 'edit_user_profile_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
		add_action( 'user_profile_update_errors', array( __CLASS__, 'validate_extra_user_profile_fields' ), 10, 3 );
		add_filter(
			'clapclap_author_social_icons',
			function( $social_icons, $author_id ) {
				$website_url = get_the_author_meta( 'user_url', (int) $author_id );
				if ( $website_url ) {
					$social_icons[] = array(
						'slug' => 'website',
						'name' => __( 'Website', 'author-social-networks' ),
						'icon' => PLUGIN_URL . '/icons/brand-world.svg',
						'link' => $website_url,
					);
				}
				$twitter_username = get_the_author_meta( TWITTER_USER_FIELD, (int) $author_id );
				if ( $twitter_username ) {
					$social_icons[] = array(
						'slug' => TWITTER_SLUG,
						'name' => TWITTER_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-twitter.svg',
						'link' => 'https://www.twitter.com/' . $twitter_username . '/',
					);
				}
				$instagram_username = get_the_author_meta( INSTAGRAM_USER_FIELD, (int) $author_id );
				if ( $instagram_username ) {
					$social_icons[] = array(
						'slug' => INSTAGRAM_SLUG,
						'name' => INSTAGRAM_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-instagram.svg',
						'link' => 'https://www.instagram.com/' . $instagram_username . '/',
					);
				}
				$facebook_username = get_the_author_meta( FACEBOOK_USER_FIELD, (int) $author_id );
				if ( $facebook_username ) {
					$social_icons[] = array(
						'slug' => FACEBOOK_SLUG,
						'name' => FACEBOOK_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-facebook.svg',
						'link' => 'https://www.facebook.com/' . $facebook_username,
					);
				}
				$tiktok_username = get_the_author_meta( TIKTOK_USER_FIELD, (int) $author_id );
				if ( $tiktok_username ) {
					$social_icons[] = array(
						'slug' => TIKTOK_SLUG,
						'name' => TIKTOK_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-tiktok.svg',
						'link' => 'https://www.tiktok.com/@' . $tiktok_username . '/',
					);
				}
				$tumblr_username = get_the_author_meta( TUMBLR_USER_FIELD, (int) $author_id );
				if ( $tumblr_username ) {
					$social_icons[] = array(
						'slug' => TUMBLR_SLUG,
						'name' => TUMBLR_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-tumblr.svg',
						'link' => 'https://' . $tumblr_username . '.tumblr.com/',
					);
				}
				$pocket_username = get_the_author_meta( POCKET_USER_FIELD, (int) $author_id );
				if ( $pocket_username ) {
					$social_icons[] = array(
						'slug' => POCKET_SLUG,
						'name' => POCKET_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-pocket.svg',
						'link' => 'https://getpocket.com/@' . $pocket_username,
					);
				}
				$patreon_username = get_the_author_meta( PATREON_USER_FIELD, (int) $author_id );
				if ( $patreon_username ) {
					$social_icons[] = array(
						'slug' => PATREON_SLUG,
						'name' => PATREON_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-patreon.svg',
						'link' => 'https://www.patreon.com/' . $patreon_username,
					);
				}
				$mastodon_url = get_the_author_meta( MASTODON_USER_FIELD, (int) $author_id );
				if ( $mastodon_url ) {
					$social_icons[] = array(
						'slug' => MASTODON_SLUG,
						'name' => MASTODON_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-mastodon.svg',
						'link' => $mastodon_url,
					);
				}
				$spotify_url = get_the_author_meta( SPOTIFY_USER_FIELD, (int) $author_id );
				if ( $spotify_url ) {
					$social_icons[] = array(
						'slug' => SPOTIFY_SLUG,
						'name' => SPOTIFY_BRAND_NAME,
						'icon' => PLUGIN_URL . '/icons/brand-spotify.svg',
						'link' => $spotify_url,
					);
				}
				return $social_icons;
			},
			10,
			2
		);
	}

	public static function is_valid_url( string $url, string $domain = '' ) : bool {
		return ( str_contains( $url, 'http://' ) || str_contains( $url, 'https://' ) ) && str_contains( $url, '.' ) && ( '' === $domain || str_contains( $url, $domain ) );
	}

	public static function is_valid_username( string $username ) : bool {
		return ! str_contains( $username, '/' );
	}

	public static function extra_user_profile_fields( WP_User $user ) {
		if ( in_array( 'author', $user->roles, true ) ) {
			?>
			<h3><?php esc_html_e( 'Social networks', 'clapclap' ); ?></h3>
			<table class="form-table">
				<tr>
					<th><label for="<?php echo esc_attr( TWITTER_USER_FIELD ); ?>"><?php esc_html_e( 'Twitter username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( TWITTER_USER_FIELD ); ?>" id="<?php echo esc_attr( TWITTER_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( TWITTER_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
							<ul>
								<li>❌ <s>https://twitter.com/clapclap_cat</s></li>
								<li>✅ clapclap_cat</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( INSTAGRAM_USER_FIELD ); ?>"><?php esc_html_e( 'Instagram username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( INSTAGRAM_USER_FIELD ); ?>" id="<?php echo esc_attr( INSTAGRAM_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( INSTAGRAM_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
							<ul>
								<li>❌ <s>https://instagram.com/clapclap_cat</s></li>
								<li>✅ clapclap_cat</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( FACEBOOK_USER_FIELD ); ?>"><?php esc_html_e( 'Facebook username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( FACEBOOK_USER_FIELD ); ?>" id="<?php echo esc_attr( FACEBOOK_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( FACEBOOK_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( TIKTOK_USER_FIELD ); ?>"><?php esc_html_e( 'TikTok username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( TIKTOK_USER_FIELD ); ?>" id="<?php echo esc_attr( TIKTOK_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( TIKTOK_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( TUMBLR_USER_FIELD ); ?>"><?php esc_html_e( 'Tumblr username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( TUMBLR_USER_FIELD ); ?>" id="<?php echo esc_attr( TUMBLR_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( TUMBLR_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( POCKET_USER_FIELD ); ?>"><?php esc_html_e( 'Pocket username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( POCKET_USER_FIELD ); ?>" id="<?php echo esc_attr( POCKET_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( POCKET_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( PATREON_USER_FIELD ); ?>"><?php esc_html_e( 'Patreon username', 'clapclap' ); ?></label></th>
					<td>
						<input type="text" name="<?php echo esc_attr( PATREON_USER_FIELD ); ?>" id="<?php echo esc_attr( PATREON_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( PATREON_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" pattern="[^()/><\][\\\x22,;|]+" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write only the username.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( MASTODON_USER_FIELD ); ?>"><?php esc_html_e( 'Mastodon URL', 'clapclap' ); ?></label></th>
					<td>
						<input type="url" name="<?php echo esc_attr( MASTODON_USER_FIELD ); ?>" id="<?php echo esc_attr( MASTODON_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( MASTODON_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write the full URL.', 'clapclap' ); ?></p>
							<ul>
								<li>❌ <s>albertjuhe</s></li>
								<li>✅ https://mastodon.social/@albertjuhe</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="<?php echo esc_attr( SPOTIFY_USER_FIELD ); ?>"><?php esc_html_e( 'Spotify URL', 'clapclap' ); ?></label></th>
					<td>
						<input type="url" name="<?php echo esc_attr( SPOTIFY_USER_FIELD ); ?>" id="<?php echo esc_attr( SPOTIFY_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( SPOTIFY_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text" />
						<div class="author-social-networks-input-explanation">
							<p><?php esc_html_e( 'Please, write the full URL.', 'clapclap' ); ?></p>
						</div>
					</td>
				</tr>
			</table>
			<?php
		}
	}

	public static function save_extra_user_profile_fields( int $user_id ) {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user_id ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		$user = get_user_by( 'ID', $user_id );

		if ( in_array( 'author', $user->roles, true ) ) {
			foreach ( self::$usernames_to_validate as $username_to_validate ) {
				if ( isset( $_POST[ $username_to_validate['field'] ] ) ) {
					$username = sanitize_text_field( wp_unslash( $_POST[ $username_to_validate['field'] ] ) );
					if ( self::is_valid_username( $username ) ) {
						update_user_meta( $user_id, $username_to_validate['field'], $username );
					}
				}
			}
			foreach ( self::$urls_to_validate as $url_to_validate ) {
				if ( isset( $_POST[ $url_to_validate['field'] ] ) ) {
					$url = esc_url_raw( wp_unslash( $_POST[ $url_to_validate['field'] ] ) );
					if ( '' === $url || self::is_valid_url( $url, $url_to_validate['domain'] ) ) {
						update_user_meta( $user_id, $url_to_validate['field'], $url );
					}
				}
			}
		}
	}

	public static function validate_extra_user_profile_fields( \WP_Error $errors, bool $update, stdClass $user ): void {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user->ID ) ) {
			return;
		}

		if ( 'author' === $user->role ) {
			foreach ( self::$usernames_to_validate as $username_to_validate ) {
				if ( array_key_exists( $username_to_validate['field'], $_POST ) ) {
					$username = sanitize_text_field( wp_unslash( $_POST[ $username_to_validate['field'] ] ) );
					if ( ! self::is_valid_username( $username ) ) {
						$error_id = 'author_social_networks_' . $username_to_validate['slug'];
						// translators: %s is the name of the social network.
						$errors->add( $error_id, sprintf( esc_html__( 'The %s username is not correct.', 'author-social-networks' ), $username_to_validate['name'] ) );
					}
				}
			}
			foreach ( self::$urls_to_validate as $url_to_validate ) {
				if ( array_key_exists( $url_to_validate['field'], $_POST ) ) {
					$url = esc_url_raw( wp_unslash( $_POST[ $url_to_validate['field'] ] ) );
					if ( '' !== $url && ! self::is_valid_url( $url, $url_to_validate['domain'] ) ) {
						$error_id = 'author_social_networks_' . $url_to_validate['slug'];
						// translators: %s is the name of the social network.
						$errors->add( $error_id, sprintf( esc_html__( 'The %s URL is not correct.', 'author-social-networks' ), $url_to_validate['name'] ) );
					}
				}
			}
		}
	}
}

$author_social_networks = new Author_Social_Networks();
add_action( 'init', array( $author_social_networks, 'init' ) );
